package it.unikey.apialvolo.controller;

import it.unikey.apialvolo.DTO.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/products")
public class ProductController {
    private Set<ProductDTO> productDTOList;

    @Autowired
    public ProductController() {
        productDTOList = new HashSet<>();
        productDTOList.add(new ProductDTO(0, "mouse", "informatica",3,new Date(2020, Calendar.SEPTEMBER,10)));
        productDTOList.add(new ProductDTO(1, "keyboard", "informatica",2,new Date(2020, Calendar.JANUARY,12)));
    }

    @GetMapping
    public ResponseEntity<Set<ProductDTO>> getAllProducts(){
        return ResponseEntity.ok(productDTOList);
    }
}
