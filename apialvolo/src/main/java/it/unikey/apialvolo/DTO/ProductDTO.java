package it.unikey.apialvolo.DTO;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Data
@NoArgsConstructor
public class ProductDTO {
    private int id;
    private String name;
    private String category;
    private int quantity;
    private Date date;

    public ProductDTO(int id, String name, String category, int quantity, Date date) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.quantity = quantity;
        this.date = date;
    }
}
