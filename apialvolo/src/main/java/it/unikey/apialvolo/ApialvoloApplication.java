package it.unikey.apialvolo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApialvoloApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApialvoloApplication.class, args);
    }

}
